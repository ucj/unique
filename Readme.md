### 基础描述

    唯一值生成器，将提供以下唯一id算法
    - 雪花算法
    - uuid(开发中)
    - 自增(开发中)

### 仓颉最低支持版本

使用此库，请使用仓颉0.53.4或者以上版本  
将不再支持0.51.4版本

### 使用此库方法

直接通过git引入项目，cjpm.toml代码如下：

```toml
unique = { git = "https://gitcode.com/ucj/unique.git" }
```

### 使用例子 - 雪花算法

```cangjie
import unique.snowflake;

main():Int64 {
    var i = 0;
    while (i < 10) {
        let sn = snowflake.next();
        println("s is ${sn}");
        i++;
    }

    0
}
```

运行结果：

```ignorelang
s is 184685938348032
s is 184685942542337
s is 184685942542338
s is 184685942542339
s is 184685946736644
s is 184685946736645
s is 184685946736646
s is 184685950930951
s is 184685950930952
s is 184685950930953
```

### 使用例子 - uuid
正在赶来的路上

### 使用例子 - 自增
正在赶来的路上
